package com.example.myapplication.Authentication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.ApiService
import com.example.myapplication.OkHttpClient
import com.example.myapplication.R
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SliderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val base_url = "https://stgtestseries.edugorilla.com"
        val my_http_client = OkHttpClient(this, base_url)
        val api_service = my_http_client.retrofit.create(ApiService::class.java)

        api_service.getUser().enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    Log.e("TAG", "onResponse: " + response.body())
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAG", "onFailure: $t")
            }
        })
    }
}
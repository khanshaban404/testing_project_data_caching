package com.example.myapplication

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("static/json/device_app_versions.json")
    fun getUser(): Call<ResponseBody>
}
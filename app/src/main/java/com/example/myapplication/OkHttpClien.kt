package com.example.myapplication

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import okhttp3.Interceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OkHttpClient(context: Context, base_url: String) {

    private val cache_size = (5 * 1024 * 1024).toLong() // 5 MB cache
    private val my_cache = Cache(context.cacheDir, cache_size)

    private val network_interceptor = Interceptor { chain ->
        val request = chain.request().newBuilder()
            .header("Cache-Control", "public, max-age=5") // 5 seconds
            .build()
        chain.proceed(request)
    }

    private val offline_interceptor = Interceptor { chain ->
        var request = chain.request()
        if (!hasNetwork(context)!!) {
            request = request.newBuilder()
                .header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7)
                .build()
        }
        chain.proceed(request)
    }

    private val okHttp_client = OkHttpClient.Builder()
        .cache(my_cache)
        .addNetworkInterceptor(network_interceptor)
        .addInterceptor(offline_interceptor)
        .build()

    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(base_url)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttp_client)
        .build()

    // Placeholder for network checking function
    private fun hasNetwork(context: Context): Boolean? {
        var is_connected: Boolean? = false //Initial Value
        val connectivity_manager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val active_network: NetworkInfo? = connectivity_manager.activeNetworkInfo
        if (active_network != null && active_network.isConnected)
            is_connected = true
        return is_connected
    }
}